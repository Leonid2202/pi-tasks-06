#pragma once
#include <string>
#include "Student.h"

using namespace std;

class Student;

class Group
{
private:
	string title;
	Student* head;
	Student** stuff;
	enum groupConstants { MAX_GROUP_SIZE = 30 };
	int nStudents;
public:
	Group();
	explicit Group(string _title);
	Group(string _title, Student* _head);
	~Group();
	void printGroupInfo();
	void setTitle(string _title);
	string getTitle();
	void setRandomHead();
	void setHead(Student* _head);
	Student* getHead();
	void addStudent(Student* _stud);
	int getNStudents();
	Student* findStudent(int _id);
	Student* findStudent(string _fio);
	void removeStudent(int _id);
	void removeStudent(string _fio);
	double getAvMark();
};

