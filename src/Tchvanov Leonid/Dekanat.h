#pragma once
#include <iostream>
#include <fstream>
#include "Student.h"
#include "Group.h"


class Dekanat
{
private:
	enum decanatConstants { MAX_NUMBER_OF_GROUPS = 30, MAX_NUMBER_OF_STUDENTS = 100 };
	Student* Students[MAX_NUMBER_OF_STUDENTS];
	Group* Groups[MAX_NUMBER_OF_GROUPS];
	int nStudents;
	int nGroups;
public:
	Dekanat();
	~Dekanat();
	void addStudent(Student* _stud);
	void addGroup(Group* _gr);
	void readStudentsFromFile(string fileName);
	void readGroupsFromFile(string fileName);
	void writeStudentsToFile(string fileName);
	void writeGroupsToFile(string fileName);
	Group* findGroup(string _title);
	Student* findStudent(string _fio);
	void randomlyDistributeStudents();
	void setRandomHeads();
	void transferStudent(Student* _st, Group* _destinationGr);
	void addRandomMarks(int nRandomMarks);
	void expulsionOfStudents(double minAvMark);
	void findBestStudents(double minMark);
	void findBestGroups(double minMark);
	void printAllGroupsInfo();

};

