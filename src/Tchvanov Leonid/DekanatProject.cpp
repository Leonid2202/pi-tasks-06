#include "stdafx.h"
#include "Student.h"
#include "Group.h"
#include "Dekanat.h"
#include <iostream>
#include <string>
#include <clocale>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	Dekanat dekanat;
	//��������� ������ � ��������� �� ������
	dekanat.readGroupsFromFile("Groups.txt");
	dekanat.readStudentsFromFile("Students.txt");
	//��������� ��������� ��������� ������, ������������ �� �������, ��������� ������� � 
	//���������� ���������� � ������� 
	dekanat.addRandomMarks(10);
	dekanat.randomlyDistributeStudents();
	dekanat.setRandomHeads();
	dekanat.printAllGroupsInfo();
	//��������� ��������� �� ������� ������ ������ 2.5 � 
	//���������� ���������� � ������� 
	cout << endl << endl << "������ ����� ���������� ���������:" << endl << endl;
	dekanat.expulsionOfStudents(2.5);
	dekanat.printAllGroupsInfo();
	//������� �� ������� ���������� � ������ ��������� � �������
	dekanat.findBestStudents(3.5);
	dekanat.findBestGroups(3.2);
	//���������� ����� ������ ����� � ��������� � �����
	dekanat.writeStudentsToFile("New students.txt");
	dekanat.writeGroupsToFile("New groups.txt");

	//������� 2 �������� ������ � ������ ��������� ��������, �������� ��������� � ������ ������, 
	//���������� ���������� � �������, ����� ��������� ��������� �������� �� ������ � ����� ���������� ���������� � �������
	Dekanat dekanat2;
	dekanat2.addGroup(new Group("Test Group 1"));
	dekanat2.addGroup(new Group("Test Group 2"));
	dekanat2.addStudent(new Student("Test Student 1", 100));
	dekanat2.findGroup("Test Group 1")->addStudent(dekanat2.findStudent("Test Student 1"));
	dekanat2.printAllGroupsInfo();
	dekanat2.transferStudent(dekanat2.findStudent("Test Student 1"), dekanat2.findGroup("Test Group 2"));
	dekanat2.printAllGroupsInfo();
	getchar();
}