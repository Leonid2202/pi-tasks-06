#pragma once
#include <string>
#include <iostream>
#include "Group.h"

using namespace std;

class Group;

class Student
{
private:
	string fio;
	int id;
	Group* gr;
	int* marks;
	int nMarks;
public:
	Student(void);
	Student(string _fio, int _id);
	Student(string _fio, int _id, Group* _gr);
	~Student(void);

	void setFio(string _fio);
	string getFio();
	void setId(int _id);
	int getId();
	void setGroup(Group* _gr);
	Group* getGroup();
	void addMark(int m);
	double getAvMark();
	void printStudentInfo();
	void printMarks(ostream& stream);
};

