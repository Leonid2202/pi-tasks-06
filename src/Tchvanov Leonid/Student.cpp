#include "stdafx.h"
#include "Student.h"
#include "Group.h"
#include <iostream>

Student::Student(void)
{
	fio = "";
	id = 0;
	gr = nullptr;
	marks = nullptr;
	nMarks = 0;
}

Student::Student(string _fio, int _id) : fio(_fio), id(_id), gr(nullptr), marks(nullptr), nMarks(0) {};

Student::Student(string _fio, int _id, Group * _gr) : fio(_fio), id(_id), gr(_gr), marks(nullptr), nMarks(0) {};

Student::~Student(void)
{
	delete[] marks;
	marks = nullptr;
}

void Student::addMark(int m)
{
	if (nMarks == 0)
	{
		marks = new int[1];
		marks[nMarks] = m;
	}
	else
	{
		int* tmp = new int[nMarks + 1];
		for (int i = 0; i < nMarks; i++)
			tmp[i] = marks[i];
		tmp[nMarks] = m;
		delete[] marks;
		marks = tmp;
	}
	nMarks++;
}

double Student::getAvMark()
{
	if (nMarks == 0)
		return 0;
	int sum = 0;
	for (int i = 0; i < nMarks; i++)
	{
		sum += marks[i];
	}
	return (double)sum/nMarks;
}

void Student::printStudentInfo()
{
	cout << "���: " << fio << endl;
	cout << "Id: " << id << endl;
	if (nMarks!= 0)
	{
		cout << "������: ";
		printMarks(cout);
		cout << "������� ������:" << getAvMark() << endl;
	}
}

void Student::printMarks(ostream& stream)
{
	for (int i = 0; i < nMarks; i++)
	{
		stream << marks[i] << " ";
	}
	stream << endl;
}

void Student::setFio(string _fio) { fio = _fio; }

string Student::getFio() { return fio; }

void Student::setId(int _id) { id = _id; }

int Student::getId() { return id; }

void Student::setGroup(Group* _gr) { gr = _gr; }

Group* Student::getGroup() { return gr; }


