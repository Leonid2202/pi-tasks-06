#include "stdafx.h"
#include "Dekanat.h"
#include "Group.h"
#include "Student.h"
#include <stdlib.h>
#include <time.h>


Dekanat::Dekanat()
{
	nStudents = 0;
	nGroups = 0;
}

Dekanat::~Dekanat()
{
	for (int i = 0; i < nStudents; i++)
	{
		delete Students[i];
	}
	for (int i = 0; i < nGroups; i++)
	{
		delete Groups[i];
	}
}

void Dekanat::addStudent(Student * _stud)
{
	if (nStudents < MAX_NUMBER_OF_STUDENTS)
	{
		Students[nStudents++] = _stud;
	}
	else
	{
		cout << "���������� ������������ ���������� ���������!" << endl;
	}
}

void Dekanat::addGroup(Group * _gr)
{
	if (nGroups < MAX_NUMBER_OF_GROUPS)
	{
		Groups[nGroups++] = _gr;
	}
	else
	{
		cout << "���������� ������������ ���������� �����!" << endl;
	}
}

void Dekanat::readStudentsFromFile(string fileName)
{
	ifstream file;
	string _fio = "";
	string _id = "";
	file.open(fileName);
	if (file.is_open())
	{
		while (!file.eof())
		{
			getline(file, _id);
			getline(file, _fio);
			Students[nStudents++] = new Student(_fio, atoi(_id.c_str()));
		}
	}
	file.close();
}

void Dekanat::readGroupsFromFile(string fileName)
{
	ifstream file;
	string _title;
	file.open(fileName);
	if (file.is_open())
	{
		while (!file.eof())
		{
			getline(file, _title);
			addGroup(new Group(_title));
		}
	}
	file.close();
}

void Dekanat::writeStudentsToFile(string fileName)
{
	ofstream file;
	file.open(fileName);
	if (file.is_open())
	{
		for (int i = 0; i < nStudents; i++)
		{
			file << Students[i]->getId() << endl;
			file << Students[i]->getFio() << endl;
			file << Students[i]->getGroup()->getTitle()<< endl;
			Students[i]->printMarks(file);
			file << endl;
		}
	}
}

void Dekanat::writeGroupsToFile(string fileName)
{
	ofstream file;
	file.open(fileName);
	if (file.is_open())
	{
		for (int i = 0; i < nGroups; i++)
		{
			file << Groups[i]->getTitle() << endl;
		}
	}
}

Group * Dekanat::findGroup(string _title)
{
	for (int i = 0; i < nGroups; i++)
	{
		if (Groups[i]->getTitle() == _title)
			return Groups[i];
	}
	cout << "��� ������ � ��������� \"" << _title << "\"!" << endl;
	return nullptr;
}

Student * Dekanat::findStudent(string _fio)
{
	for (int i = 0; i < nStudents; i++)
	{
		if (Students[i]->getFio() == _fio)
			return Students[i];
	}
	cout << "� ������������ ��� �������� � ��� \"" << _fio << "\" !" << endl;
	return nullptr;
}

void Dekanat::randomlyDistributeStudents()
{
	srand((int)time(NULL));
	for (int i = 0; i < nStudents; i++)
	{
		Students[i]->setGroup(Groups[rand() % nGroups]);
		Students[i]->getGroup()->addStudent(Students[i]);
	}
}

void Dekanat::setRandomHeads()
{
	for (int i = 0; i < nGroups; i++)
	{
		Groups[i]->setRandomHead();
	}
}

void Dekanat::transferStudent(Student * _st, Group * _destinationGr)
{
	_st->getGroup()->removeStudent(_st->getId());
	_destinationGr->addStudent(_st);
}

void Dekanat::addRandomMarks(int nRandomMarks)
{
	srand((int)time(NULL));
	for (int i = 0; i < nStudents; i++)
	{
		for (int j = 0; j < nRandomMarks; j++)
			Students[i]->addMark(rand() % 5 + 1);
	}
}

void Dekanat::expulsionOfStudents(double minAvMark)
{
	int i = 0;
	while (i < nStudents)
	{
		if (Students[i]->getAvMark() < minAvMark)
		{
			Students[i]->getGroup()->removeStudent(Students[i]->getFio());
			delete Students[i];
			Students[i] = Students[--nStudents];

		}
		else
			i++;
	}
}

void Dekanat::findBestStudents(double minMark)
{

	cout << "������ ������� ������������ (��� - ������� ������): " << endl << endl;
	for (int i = 0; i < nStudents; i++)
	{
		if (minMark <= Students[i]->getAvMark())
			cout << Students[i]->getFio() << " - " << Students[i]->getAvMark() << endl;
	}
	cout << endl << endl;
}

void Dekanat::findBestGroups(double minMark)
{
	cout << "������ ������ ������������ (�������� ������ - ������� ������):" << endl << endl;
	for (int i = 0; i < nGroups; i++)
	{
		if (minMark <= Groups[i]->getAvMark())
			cout << Groups[i]->getTitle() << " - " << Groups[i]->getAvMark() << endl;
	}
	cout << endl;
}

void Dekanat::printAllGroupsInfo()
{
	for (int i = 0; i < nGroups; i++)
	{
		Groups[i]->printGroupInfo();
	}
	cout << "====================================================" << endl;
}
