#include "stdafx.h"
#include "Group.h"
#include <string>
#include <time.h>

Group::Group()
{
	title = "";
	head = nullptr;
	stuff = new Student*[MAX_GROUP_SIZE];
	nStudents = 0;
}

Group::Group(string _title)
{
	title = _title;
	head = nullptr;
	stuff = new Student*[MAX_GROUP_SIZE];
	nStudents = 0;
}

Group::Group(string _title, Student *_head)
{
	title = _title;
	head = _head;
	stuff = new Student*[MAX_GROUP_SIZE];
	stuff[0] = head;
	nStudents = 1;
}

Group::~Group()
{
	delete[] stuff;
	stuff = nullptr;
}

void Group::printGroupInfo()
{
		cout << "���������� � ������ " << title << ":" << endl << endl;
		cout << "���������� ��������� � ������: " << nStudents << endl;
		if (head != nullptr)
		{
			cout << "�������� ������: " << head->getFio() << endl;
		}
		else
		{
			cout << "�������� �� ��������" << endl;
		}
		cout << "������� ���� � ������: " << getAvMark() << endl << endl;
		cout << "���������� � ���������:" << endl;
		for (int i = 0; i < nStudents; i++)
		{
			stuff[i]->printStudentInfo();
			cout << "----------------------------------------------------" << endl;
		}
		cout << endl;
}

void Group::addStudent(Student *_stud)
{
	if (nStudents < MAX_GROUP_SIZE)
	{
		stuff[nStudents++] = _stud;
		_stud->setGroup(this);
	}
	else
		cout << "���������� ������������ ���������� ��������� � ������!" << endl;
}

Student* Group::findStudent(int _id)
{
	for (int i = 0; i < nStudents; i++)
	{
		if (stuff[i]->getId() == _id)
			return stuff[i];
	}
	cout << "� ������ ��� �������� � id = " << _id << " !" << endl;
	return nullptr;
}

Student* Group::findStudent(string _fio)
{
	for (int i = 0; i < nStudents; i++)
	{
		if (stuff[i]->getFio() == _fio)
			return stuff[i];
	}
	cout << "� ������ ��� �������� � ��� \"" << _fio << "\" !" << endl;
	return nullptr;
}

void Group::removeStudent(int _id)
{
	Student *remStud = findStudent(_id);
	if (remStud != nullptr)
	{
		int i = 0;
		while (stuff[i++]->getId() != remStud->getId());
		stuff[i - 1] = stuff[nStudents - 1];
		if (head == remStud)
		{
			head = nullptr;
		}
		nStudents--;
	}
}

void Group::removeStudent(string _fio)
{
	Student *remStud = findStudent(_fio);
	if (remStud != nullptr)
	{
		int i = 0;
		while (stuff[i++]->getId() != remStud->getId());
		stuff[i - 1] = stuff[nStudents - 1];
		if (head == remStud)
		{
			head = nullptr;
		}
		nStudents--;
	}
}

double Group::getAvMark()
{
	if (nStudents == 0)
		return 0;
	double sum = 0;
	for (int i = 0; i < nStudents; i++)
	{
		sum += stuff[i]->getAvMark();
	}
	return (double)sum / nStudents;
}

void Group::setTitle(string _title) { title = _title; }

string Group::getTitle() { return title; }

void Group::setRandomHead()
{
	srand((int)time(NULL));
	if (nStudents != 0)
		head = stuff[rand() % nStudents];
}

void Group::setHead(Student *_head) { head = _head; }

Student* Group::getHead() { return head; }

int Group::getNStudents() { return nStudents; }